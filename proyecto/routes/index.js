
var express = require('express');
var router = express.Router();
//Usuario(Cuenta)
var usuarioC = require('../controladores/ControladorUsuario');
var usuario = new usuarioC();
//Persona
var personaC = require('../controladores/ControladorPersona');
var persona = new personaC();

//Album
var albumC = require('../controladores/ControladorAlbum');
var album = new albumC();

//Cancion
var cancionC = require('../controladores/ControladorCancion');
var cancion = new cancionC();
//Lista_canciones
var lista_cancionesC = require('../controladores/ControladorLista_canciones');
var lista_canciones = new lista_cancionesC();
//Lista_reproduccion
var lista_reproduccionC = require('../controladores/ControladorLista_reproduccion');
var lista_reproduccion = new lista_reproduccionC();

var administrador = require('../controladores/ControladorAdministardor');
var admin = new administrador();
//Utilidades
var utilidades = require('../public/javascripts/Utilidades');
//Pagos
var planC = require('../controladores/ControladorPlan');
var plan = new planC();
//Passport
var passport = require('passport');
//

//Middlewar
var auth = function (req, res, next) {
    if (req.isAuthenticated()) {
        next();
    } else {
        req.flash('error', 'Debes iniciar sesión primero');
        res.redirect('/');
    }
};
//
/* GET home page. */
router.get('/', usuario.visualizar_principal);

//USUARIO
router.get('/registro', function (req, res, next) {
    res.render('index', {title: 'Registro', msg: {error: req.flash('error'), info: req.flash('info')}, fragmentos: "Registro_usuario"});//llamar la variable
});

//ALBUM
router.get('/album', auth,album.visualizar_album);
router.get('/modificar_portada/:external/:nombre', auth,album.visualizar_portada_album);
router.post('/guardar_album', auth,album.crear_album);
router.post('/guardar_portada', auth,album.guardar_portada);
router.get('/buscar_albumes', auth, album.buscar_albumes);

router.get('/crear_album', auth, function (req, res, next) {
    res.render('index', {title: 'Aureo | Crear Album', msg: {error: req.flash('error'), info: req.flash('info')}, external: req.user.id, fragmentos: "CrearAlbum"});
});
//CANCION
router.get('/cancion', auth, cancion.visualizar_cancion_persona);
router.get('/nueva_cancion', auth,cancion.visualizar_subircancion);
router.post('/guardar_cancion', auth ,cancion.guardar_cancion);
router.get('/nuevas_canciones', auth, cancion.visualizar_nuevas_canciones);
//BANDA
router.get('/banda', auth, function (req, res, next) {
    res.render('index', {title: 'Aureo | Mi Banda', msg: {error: req.flash('error'), info: req.flash('info')}, fragmentos: "Bandas"});
});
//TOP 10
router.get('/top10', auth, cancion.visualizar_top_10);
//HISTORIAL
router.get('/historial', auth, function (req, res, next) {
    res.render('index', {title: 'Aureo | Historial', msg: {error: req.flash('error'), info: req.flash('info')}, fragmentos: "Historial"});
});
//LISTA REPRODUCCION
router.get('/lista_reproduccion', auth, lista_reproduccion.visualizar_listar);
router.get('/modificar_caratula/:external', auth,lista_reproduccion.visualizar_caratula_listar);
router.post('/crear_listaR', auth,lista_reproduccion.crear_listar);
router.post('/guardar_caratula', auth,lista_reproduccion.guardar_caratula);
router.get('/ver_lista/:external', auth, lista_reproduccion.visualizar_contenido_listar);
router.get('/crear_listar', auth, function (req, res, next) {
    res.render('index', {title: 'Aureo | Crear Lista de Reproducion', external: req.user.id, fragmentos: "CrearListaReproduccion"});
});
//LISTA CANCIONES
router.post('/agregar_lista', auth,lista_canciones.agregar_lista);
router.post('/eliminar_de_lista', auth,lista_canciones.eliminar_de_lista);

//Pagos
router.get('/plan', auth,plan.visualizar);
router.get('/tarjeta/:external', auth,plan.visualizar_tar);
router.post('/guardar/plan',auth,plan.guardar);
//ADMINISTRACION
router.get('/administrar', auth, function (req, res, next) {
    if (req.user.rol === "administrador")
        res.render('index', {title: 'Aureo | Administración', fragmentos: "Administracion"});
    else {
        req.flash('error', 'No tienes permiso para acceder a este sitio');
        res.redirect('/');
    }
});
router.get('/administrarUsuarios', auth, admin.listar_usuarios);
router.get('/administrarCanciones', auth, admin.listar_canciones);
//Passport (Iniciar Sesion)
router.post('/inicio_sesion',
        passport.authenticate('local-signin',
                {successRedirect: '/',
                    failureRedirect: '/',
                    failureFlash: true}
        ));
//
//Cerrar sesion
router.get('/cerrar_sesion', auth, usuario.cerrar_sesion);
//

//Persona
router.post('/registro', persona.guardar);
router.get('/perfil', auth, persona.visualizar_perfil);
router.get('/modificar_perfil', auth, persona.visualizar_modificar);
router.post('/guardar_modificar_perfil', auth, persona.modificar_perfil);
router.get('/modificar_foto_perfil/:external', auth,persona.visualizar_foto_perfil);
router.post('/guardar_foto', auth,persona.guardar_foto);

module.exports = router;