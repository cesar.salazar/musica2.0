var bcrypt = require('bcrypt-nodejs'); //modulo para encriptar claves
var models = require('./../models');

var cuenta = models.usuario;
var persona = models.persona;
var rol = models.rol;
var artista = models.artista;
module.exports = function (passport) {
    var Cuenta = cuenta;//modelo
    var Persona = persona;//modelo
    var Rol = rol;
    var Artista = artista;
    var LocalStrategy = require('passport-local').Strategy;
    //Permite serializar los datos de cuenta
    passport.serializeUser(function (cuenta, done) {
        done(null, cuenta.id);
    });

    // Permite deserialize la cuenta de usuario
    passport.deserializeUser(function (id, done) {
        Cuenta.findOne({where: {id: id}, include: [{model: Persona, include: [{model: Rol},{model:Artista, as: 'artista'}]}]}).then(function (cuenta) {
            if (cuenta) {
//                var c=stringify(cuenta);
                var userinfo = {
                    id: cuenta.persona.external_id,
                    nombre: cuenta.persona.apellidos + " " + cuenta.persona.nombres,
                    rol: cuenta.persona.rol.nombre_rol,
                    artista: cuenta.persona.artista.nom_artista
                };
                
                done(null, userinfo);
            } else {
                done(cuenta.errors, null);
            }
        });
    });
    //inicio de sesion
    passport.use('local-signin', new LocalStrategy(
            {
                usernameField: 'correo',
                passwordField: 'clave',
                passReqToCallback: true // allows us to pass back the entire request to the callback
            },
            function (req, correo, password, done) {
                var Cuenta = cuenta;
                Cuenta.findOne({where: {correo: correo}}).then(function (cuenta) {
                    if (!cuenta) {
                        return done(null, false, {message: req.flash('error', 'No existe una cuenta con ese correo')});
                    }
                    if (cuenta.clave !== password) {
                        return done(null, false, {message: req.flash('error', 'Clave incorrecta')});
                    }
                    var userinfo = cuenta.get();
                    return done(null, userinfo);
                }).catch(function (err) {
                    return done(null, false, {message: req.flash('error', 'Cuenta erronea')});
                });
            }
    ));
};


