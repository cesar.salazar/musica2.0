var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
//para los fragmentos
var hbs = require('express-handlebars');
var logger = require('morgan');
//Declaracion variables de Session y Flash
var session = require('express-session');
var flash = require('connect-flash');
//
//Declaracion variable de Passport
var passport = require('passport');
//
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var adminRouter = require('./routes/admin');
var app = express();

app.engine('hbs',hbs({extname: 'hbs' , defaultLayout:'layout' , layoutsDir: __dirname + '/views/fragmentos/'}));
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

///Aqui va la configuracion del Session
app.use(session({
    secret: 'ProyectoMusicalxd',
    resave: false,
    saveUninitialized: true
}));
///

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//Configuracion Flash
app.use(flash());
//
//Configuracion Passport
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
require('./config/passport')(passport);
//

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/admin', adminRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
