'use strict';
var models = require('../../models');
var uuid = require('uuid');
const bcrypt = require('bcrypt-nodejs');
const saltRounds = 8;

/**
 * @function crear_roles
 * @description Permite crear todos los roles y además crea un usuario Admin por defecto
 * @returns {undefined}
 */
function crear_roles() {
    var rol = models.rol;
    rol.findAll().then(function (lista) {
        if (lista.length === 0) {
            rol.bulkCreate([
                {nombre_rol: 'administrador'},
                {nombre_rol: 'usuario'},
                {nombre_rol: 'premium'}
            ]).then(() => {
                return rol.findOne({where: {nombre_rol: 'administrador'}});
            }).then(rolAdmin => {
                var persona = models.persona;
                var generateHash = function (clave) {
                    return bcrypt.hashSync(clave, bcrypt.genSaltSync(saltRounds), null);
                };
                var data = {
                    apellidos: "Administrador",
                    nombres: "Administrador",
                    fecha_nac: "1999-04-20",
                    telefono: "0939694489",
                    foto: "logob.png",
                    external_id: uuid.v4(),
                    id_rol: rolAdmin.id,
                    usuario: {
                        correo: 'admin@admin.com',
                        clave: generateHash('1234'),
                        estado: true,
                        external_id: uuid.v4()
                    },
                    artista: {
                        nom_artista: 'Admin',
                        tipo_artista: 'Administrador',
                        external_id: uuid.v4()
                    }
                };
                persona.create(data, {include: [{model: models.usuario, as: 'usuario'}, {model: models.artista, as: 'artista'}]});
            });
        }
    });
}

module.exports = {crear_roles};