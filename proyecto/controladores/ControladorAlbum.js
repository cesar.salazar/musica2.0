'use strict';
var models = require('./../models/');
var album = models.album;
var artista = models.artista;
var persona = models.persona;
var uuid = require('uuid');
var formidable = require('formidable');
var extensiones = ["jpg", "png", "jpeg"];
var maxSize = 20 * 1024 * 1024;
var fs = require('fs');

/**
 * @class ControladorAlbum
 */
class ControladorAlbum {
    
    /**
     * @description Método para visualizar todos los albumes que la persona haya creado, y también para que redireccione a la ventana Albumes
     * @param {req} req Sirve para obtener el nombre del artista y poder presentar solo los albumes de ese artista
     * @param {res} res Sirve para redireccionar a la página de Albumes, o en el caso de que haya algún error a la página principal
     */
    visualizar_album(req, res) {
        var artista_nombre = req.user.artista;
        album.findAll({include: [{model: artista, as: 'artistum', where: {nom_artista: artista_nombre}}]}).then(function (datos) {
            res.render('index',
                    {
                        title: 'Aureo | Album',
                        fragmentos: "Albumes",
                        info: datos,
                        msg: {error: req.flash('error'), info: req.flash('info')}
                        
                    });
        }).error(function (error) {
            req.flash('error', 'Hubo un problema, comunicate con tu servicio del sistema');
            res.redirect('/');
        });
    }
    
    /**
     * @description Método para visualizarla ventana de EditarPortadaAlbum, y así poder subir la nueva foto para la portada
     * @param {req} req Sirve para presentar los mensajes respectivos de información o de error, también para obtener el external id del album, para poderlo filtrar; y el nombre del álbum para poderle dar un nombre después
     * @param {res} res Sirve para presentarle al usuario la ventana de EditarPortadaAlbum y en el caso de que haya algún error, redireccionar a la principal
     */
    visualizar_portada_album(req, res) {
        var external = req.params.external;
        var nombre = req.params.nombre;
        album.findOne({where: {external_id: external}}).then(function (Datos) {
            res.render('index',
                    {
                        title: 'Aureo | Editar Portada del Album',
                        fragmentos: "EditarPortadaAlbum",
                        info: Datos,
                        external: external,
                        nombre: nombre,
                        msg: {error: req.flash('error'), info: req.flash('info')}
                        
                    });
        }).error(function (error) {
            req.flash('error', 'Hubo un problema, comunicate con tu servicio del sistema');
            res.redirect('/');
        });
    }
    
    /**
     * @description Método para crear el álbum obteniendo todos sus datos y guardandolos en la base de datos
     * @param {req} req Sirve para presentar los mensajes respectivos de información o de error, también para obtener el external id de la persona y los datos que el usuario haya escrito acerca del álbum.
     * @param {res} res Sirve para redireccionar a la ventana Albumes si todo sale bien, y en el caso de error a la de CrearAlbum
     */
    crear_album(req, res) {
        var external_persona = req.user.id;
        var nombre = req.body.nombre;
        persona.findOne({where: {external_id: external_persona}, include: [{model: artista, as: 'artista'}]}).then(function (Datos) {
            album.findOne({where: {nom_album: nombre}}).then(function (DatosAlbum) {
                
                
                if (DatosAlbum === null) {
                    var datos = {
                        nom_album: req.body.nombre,
                        portada: 'logon.png',
                        id_artista: Datos.artista.id,
                        external_id: uuid.v4()
                    };
                    album.create(datos).then(function (AlbumDatos) {
                        req.flash('info', "Se ha creado correctamente el album");
                        res.redirect("/album");
                    });
                } else {
                    req.flash('error', 'Ese nombre ya ha sido usado');
                    res.redirect('/crear_album');
                }
            });
        });
    }
    
    /**
     * @description Método para cambiar la imagen de portada del album
     * @param {req} req Sirve para presentar los mensajes respectivos de información o de error, también para obtener la imagen que el usuario haya subido en el input llamado "portada"
     * @param {res} res Sirve para redireccionar a la ventana Album en el caso de que haya un error o de que todo salga bien
     */
    guardar_portada(req, res) {
        var form = new formidable.IncomingForm();
        form.parse(req, function (err, fields, files) {
            var portada = files.portada.name;
            if (files.portada.size <= maxSize) {
                var extension = files.portada.name.split(".").pop().toLowerCase();
                if (extensiones.includes(extension)) {
                    var external = fields.external;
                    var nombre = quitarespacios(fields.nombre);
                    var nombreFoto = external + nombre + "." + extension;
                    
                    fs.rename(files.portada.path, "public/portadas/" + nombreFoto,
                            function (err) {
                                if (err) {
                                    req.flash('error', "Hubo un error " + err);
                                    res.redirect("/album");
                                } else {
                                    album.findOne({where: {external_id: external}}).then(function (result) {
                                        result.portada = nombreFoto;
                                        result.save().then(function (ok) {
                                            fs.exists(files.portada.path, function (exists) {
                                                if (exists)
                                                    fs.unlinkSync(files.portada.path);
                                            });
                                            req.flash('info', "Se ha cambiado su foto de perfil");
                                            res.redirect("/album");
                                        });
                                        
                                    });
                                }
                            });
                    
                    
                } else {
                    req.flash('error', "El tipo de archivo no es valido, debe ser png, jpg, o jpeg");
                    res.redirect("/album");
                }
            } else {
                req.flash('error', "El tamano no puede superar 1 MB");
                res.redirect("/album");
            }
        });
    }
    
    /**
     * @description Método para buscar todos los albumes que le pertenezcan solo al usuario
     * @param {req} req Sirve para presentar los mensajes respectivos de información o de error, y también obtener el nombre del artista guardado en el inicio de sesión con passport
     * @param {res} res Sirve para presentarle al usuario la ventana de Albumes, y en el caso de error que redireccione a la pantalla principal
     */
    buscar_albumes(req, res) {
        var artista_nombre = req.user.artista;
        album.findAll({include: [{model: artista, as: 'artistum', where: {nom_artista: artista_nombre}}]}).then(function (datos) {
            res.render('index',
                    {
                        title: 'Aureo | Album',
                        fragmentos: "Albumes",
                        data: datos,
                        msg: {error: req.flash('error'), info: req.flash('info')}
                        
                    });
        }).error(function (error) {
            req.flash('error', 'Hubo un problema, comunicate con tu servicio del sistema');
            res.redirect('/');
        });
    }
}

/**
 * @description Funcion para quitarle los espacios al nombre
 * @param {str} str Obtiene un String al cual le quitará los espacios
 * @returns {quitarespacios.cadena|String} Retorna el String sin espacios
 */
function quitarespacios(str) {
    var cadena = '';
    var arrayString = str.split(' ');
    for (var i = 0; i < arrayString.length; i++) {
        if (arrayString[i] != "") {
            cadena += arrayString[i];
        }
    }
    return cadena;
}
;

module.exports = ControladorAlbum;

