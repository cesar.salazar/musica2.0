'use strict';
var models = require('./../models/');
var uuid = require('uuid');

/**
 * @class ControladorAdministrador
 */
class ControladorAdministrador {
    /**
     * @description Método para listar todos los usuarios registrados dentro de la plataforma
     * @param {req} req Sirve para presentar los mensajes respectivos de información o de error
     * @param {res} res Sirve para presentarle al usuario la ventana de Administración de Usuarios
     */
    listar_usuarios(req, res) {
        var persona = models.persona;
        if (req.user.rol === 'administrador') {
            persona.findAll({include: [{model: models.usuario, as: 'usuario'}, {model: models.artista, as: 'artista'}]}).then(function (datos) {
                res.render('index',
                        {
                            title: 'Aureo | Administracion Usuarios',
                            fragmentos: "Administracion_Usuarios",
                            info: datos,
                            admin: true,
                            msg: {error: req.flash('error'), info: req.flash('info')}

                        });
            }).error(function (error) {
                req.flash('error', 'Hubo un problema, comunicate con tu servicio del sistema');
                res.redirect('/');
            });
        } else {
            req.flash('error', 'No tienes permiso para acceder a este sitio');
            res.redirect('/');
        }
    }

    /**
     * @description Método para listar todos las canciones creadas dentro de la plataforma
     * @param {req} req Sirve para presentar los mensajes respectivos de información o de error
     * @param {res} res Sirve para presentarle al usuario la ventana de Administrar Canciones
     */
    listar_canciones(req, res) {
        var art_alb_can = models.art_alb_can;
        if (req.user.rol === 'administrador') {
            art_alb_can.findAll({include: [{model: models.album, as: 'album'}, {model: models.artista, as: 'artistum'}, {model: models.cancion, as: 'cancion'}]}).then(function (datos) {
                res.render('index',
                        {
                            title: 'Aureo | Administracion de Canciones',
                            fragmentos: "Administrar_Canciones",
                            info: datos,
                            admin: true,
                            msg: {error: req.flash('error'), info: req.flash('info')}

                        });
            }).error(function (error) {
                req.flash('error', 'Hubo un problema, comunicate con tu servicio del sistema');
                res.redirect('/');
            });
        } else {
            req.flash('error', 'No tienes permiso para acceder a este sitio');
            res.redirect('/');
        }
    }
}
module.exports = ControladorAdministrador;

