'use strict';
var models = require('./../models/');
var uuid = require('uuid');

/**
 * @class ControladorPlan
 */
class ControladorPlan {
    /**
     * @description Permite visualizar la ventana de Plan, en la cuál se podrá pagar por el plan premium
     * @param {req} req Obtiene el external id de la persona, y también presenta los mensajes de información o de error
     * @param {res} res Direcciona a la ventana de Plan, y en el caso de que exista algún error, lo redirige a la ventana principal 
     * @returns {undefined}
     */
    visualizar(req, res) {
        var pagos = models.pagos;
        var persona = models.persona;
        var external_persona = req.user.id;
        persona.findAll({include: [{model: pagos, as: 'pagos'}]}).then(function (datos) {
            if (datos) {
                res.render('index',
                        {
                            title: 'Aureo | Canciones',
                            fragmentos: "Plan",
                            info: datos,
                            external: external_persona,
                            msg: {error: req.flash('error'), info: req.flash('info')}

                        });
            } else {
                req.flash('error', 'Presentamos problemas');
                res.redirect('/');
            }
        }).error(function (error) {
            req.flash('error', 'Temporalmente no se puede obtener premin');
            res.redirect('/');
        });
    }

    /**
     * @description Permite visualizar la ventana de tarjeta, en la cual pondrá sus datos personales de la tarjeta de crédito
     * @param {req} req Obtiene el external id de la persona, y también presenta los mensajes de información o de error
     * @param {res} res Direcciona a la ventana de Tarjeta, y en el caso de que exista algún error, lo redirige a la ventana principal 
     * @returns {undefined}
     */
    visualizar_tar(req, res) {
        var pagos = models.pagos;
        var persona = models.persona;
        var external = req.params.external;
        persona.findAll({where: {external_id: external}, include: [{model: pagos, as: 'pagos'}]}).then(function (datos) {
            if (datos) {
                res.render('index',
                        {
                            title: 'Aureo | Canciones',
                            fragmentos: "Tarjeta",
                            info: datos,
                            msg: {error: req.flash('error'), info: req.flash('info')}

                        });
                console.log(datos);
            } else {
                req.flash('error', 'Presentamos problemas');
                res.redirect('/');
            }
        }).error(function (error) {
            req.flash('error', 'Temporalmente no se puede obtener premin');
            res.redirect('/');
        });
    }

    /**
     * @description Permite guardar todos los datos del plan junto con el usuario en la base de datos
     * @param {req} req Obtiene todos los datos del plan de pagos
     * @param {res} res Redirecciona a la venta Perfil en el caso de que todo se ejecute correctamente
     * @returns {undefined}
     */
    guardar(req, res) {
        var pagos = models.pagos;
        var persona = models.persona;
        persona.findOne({include: [{model: pagos, as: 'pagos'}]}).then(function (datospagos) {
            if (datospagos) {

                var datosrelacionados = {
                    costo: req.body.costo,
                    fecha: req.body.fechas,
                    nombre_plan: req.body.plan,
                    id_persona: datospagos.id,
                    external_id: uuid.v4()
                };
                pagos.create(datosrelacionados, {include: [{model: models.persona}]}).then(function (lista) {
                    req.flash('info', "Ha solicitado el plan Premium, se habilitará en unas horas...");
                    res.redirect("/Perfil");
                });


            }
        });
    }

}


module.exports = ControladorPlan;

