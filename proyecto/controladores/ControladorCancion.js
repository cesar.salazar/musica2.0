'use strict';
var models = require('./../models/');
var uuid = require('uuid');
var album = models.album;
var artista = models.artista;
var persona = models.persona;
var listaR = models.lista_reprod;
var cancion = models.cancion;
var art_alb_can = models.art_alb_can;
var formidable = require('formidable');
var extensiones = ["mp3", "flac", "m4a", "wav"];
var maxSize = 25 * 1024 * 1024;
var fs = require('fs');

/**
 * @class ControladorCancion
 */
class ControladorCancion {

    /**
     * @description Método para visualizar la ventana de NuevasCanciones, en la cuál estarán todas las canciones que los usuarios hayan subido
     * @param {req} req Sirve para obtener el nombre del artista y el external id de la persona, y también para presentar los mensajes de error o de información
     * @param {res} res Sirve para presentar la ventana de NuevasCanciones, y el caso de que haya algún error, redireccionar al menú principal
     */
    visualizar_nuevas_canciones(req, res) {
        var external = req.user.id;
        var artista_nombre = req.user.artista;
        var rol = req.user.rol;

        if (rol === 'administrador') {
            art_alb_can.findAll({include: [{model: album, as: 'album'}, {model: artista, as: 'artistum'}, {model: cancion, as: 'cancion'}]}).then(function (datos) {
                if (datos) {
                    listaR.findAll({include: [{model: persona, where: {external_id: external}}]}).then(function (datos_lista) {

                        if (datos_lista) {
                            res.render('index',
                                    {
                                        title: 'Aureo | Nuevas Canciones',
                                        fragmentos: "NuevasCanciones",
                                        info: datos,
                                        premium: true,
                                        lista: datos_lista,
                                        msg: {error: req.flash('error'), info: req.flash('info')}

                                    });
                        } else {
                            req.flash('error', 'Hubo un problema, comunicate con tu servicio del sistema');
                            res.redirect('/');
                        }
                    }).error(function (error) {
                        req.flash('error', 'Hubo un problema, comunicate con tu servicio del sistema');
                        res.redirect('/');
                        ;
                    });
                } else {
                    req.flash('error', 'Hubo un problema, comunicate con tu servicio del sistema');
                    res.redirect('/');
                }

            }).error(function (error) {
                req.flash('error', 'Hubo un problema, comunicate con tu servicio del sistema');
                res.redirect('/');
            });
        } else if (rol === 'premium') {
            art_alb_can.findAll({include: [{model: album, as: 'album'}, {model: artista, as: 'artistum'}, {model: cancion, as: 'cancion'}]}).then(function (datos) {
                if (datos) {
                    listaR.findAll({include: [{model: persona, where: {external_id: external}}]}).then(function (datos_lista) {

                        if (datos_lista) {
                            res.render('index',
                                    {
                                        title: 'Aureo | Nuevas Canciones',
                                        fragmentos: "NuevasCanciones",
                                        info: datos,
                                        lista: datos_lista,
                                        p: true,
                                        msg: {error: req.flash('error'), info: req.flash('info')}
                                    });
                        } else {
                            req.flash('error', 'Hubo un problema, comunicate con tu servicio del sistema');
                            res.redirect('/');
                        }
                    }).error(function (error) {
                        req.flash('error', 'Hubo un problema, comunicate con tu servicio del sistema');
                        res.redirect('/');
                        ;
                    });
                } else {
                    req.flash('error', 'Hubo un problema, comunicate con tu servicio del sistema');
                    res.redirect('/');
                }

            }).error(function (error) {
                req.flash('error', 'Hubo un problema, comunicate con tu servicio del sistema');
                res.redirect('/');
            });
        } else if (rol === 'usuario') {
            art_alb_can.findAll({include: [{model: album, as: 'album'}, {model: artista, as: 'artistum'}, {model: cancion, as: 'cancion'}]}).then(function (datos) {
                if (datos) {
                    listaR.findAll({include: [{model: persona, where: {external_id: external}}]}).then(function (datos_lista) {

                        if (datos_lista) {
                            res.render('index',
                                    {
                                        title: 'Aureo | Nuevas Canciones',
                                        fragmentos: "NuevasCanciones",
                                        info: datos,
                                        lista: datos_lista,
                                        msg: {error: req.flash('error'), info: req.flash('info')}

                                    });
                        } else {
                            req.flash('error', 'Hubo un problema, comunicate con tu servicio del sistema');
                            res.redirect('/');
                        }
                    }).error(function (error) {
                        req.flash('error', 'Hubo un problema, comunicate con tu servicio del sistema');
                        res.redirect('/');
                        ;
                    });
                } else {
                    req.flash('error', 'Hubo un problema, comunicate con tu servicio del sistema');
                    res.redirect('/');
                }

            }).error(function (error) {
                req.flash('error', 'Hubo un problema, comunicate con tu servicio del sistema');
                res.redirect('/');
            });
        }

    }

    /**
     * @description Método para visualizar la ventana de Top10, el cuál se limitará a 5 (Se ha limitado a 5 debido que habría que crear más de 10 registros para poder visualizar lo que hace el top, pero con solo editarle el limit ya presentaría 10), según las veces que se hayan añadido a una lista de reproducción
     * @param {req} req Sirve para obtener el nombre del artista y el external id de la persona, y también para presentar los mensajes de error o de información
     * @param {res} res Sirve para presentar la ventana de Top10, y en el caso de que haya algún error redirecciona a la ventana principal
     */
    visualizar_top_10(req, res) {
        var external = req.user.id;
        cancion.findAll({
            order: [["reproducciones", "DESC"]],
            limit: 5,
            offset: 0,
            include: [album]
        }).then(function (datos) {
            if (datos) {
                listaR.findAll({include: [{model: persona, where: {external_id: external}}]}).then(function (datos_lista) {

                    if (datos_lista) {
                        res.render('index',
                                {
                                    title: 'Aureo | Top 5',
                                    fragmentos: "Top10",
                                    info: datos,
                                    lista: datos_lista,
                                    msg: {error: req.flash('error'), info: req.flash('info')}

                                });
                    } else {
                        req.flash('error', 'Hubo un problema, comunicate con tu servicio del sistema');
                        res.redirect('/');
                    }
                }).error(function (error) {
                    req.flash('error', 'Hubo un problema, comunicate con tu servicio del sistema');
                    res.redirect('/');
                    ;
                });
            } else {
                req.flash('error', 'Hubo un problema, comunicate con tu servicio del sistema');
                res.redirect('/');
            }
        }).error(function (error) {
            req.flash('error', 'Hubo un problema, comunicate con tu servicio del sistema');
            res.redirect('/');
            ;
        });

    }

    /**
     * @description Método para visualizar las canciones que haya subido el usuario o artista a la plataforma
     * @param {req} req Permite obtener el external id de la persona, el nombre del artista, y presentar un mensaje de información o de error
     * @param {res} res Permite direccionar a la ventana de Canciones y el caso de que haya algún error redireccionar a la ventana principal
     */
    visualizar_cancion_persona(req, res) {
        var external = req.user.id;
        var artista_nombre = req.user.artista;
        art_alb_can.findAll({include: [{model: album, as: 'album'}, {model: artista, as: 'artistum', where: {nom_artista: artista_nombre}}, {model: cancion, as: 'cancion'}]}).then(function (datos) {
            if (datos) {
                res.render('index',
                        {
                            title: 'Aureo | Canciones',
                            fragmentos: "Canciones",
                            info: datos,
                            msg: {error: req.flash('error'), info: req.flash('info')}

                        });

            } else {
                req.flash('error', 'Hubo un problema, comunicate con tu servicio del sistema');
                res.redirect('/');
            }
        }).error(function (error) {
            req.flash('error', 'Hubo un problema, comunicate con tu servicio del sistema');
            res.redirect('/');
        });
    }

    /**
     * @description Método para visualizar la Ventana de SubirCancion, en la cuál se podrá subir el archivo de audio junto con todos sus datos
     * @param {req} req Permite obtener el nombre del artista, y presentar los respectivos mensaje en el caso de que haya error o de que todo vaya bien
     * @param {res} res Permite visualizar la ventana de SubirCancion, y en el caso de que se produzca algún error que redireccione la ventana Cancion
     * @returns {undefined}
     */
    visualizar_subircancion(req, res) {
        var artista_nombre = req.user.artista;
        album.findAll({include: [{model: artista, as: 'artistum', where: {nom_artista: artista_nombre}}]}).then(function (datos) {
            res.render('index',
                    {
                        title: 'Aureo | Subir Canción',
                        fragmentos: "SubirCancion",
                        info: datos,
                        msg: {error: req.flash('error'), info: req.flash('info')}

                    });
        }).error(function (error) {
            req.flash('error', 'Hubo un problema, comunicate con tu servicio del sistema');
            res.redirect('/cancion');
        });
    }

    /**
     * @description Método para Guardar todos los datos de la canción en la base datos y de igual manera la direccion del archivo mp3 
     * @param {req} req Permite obtener el nombre del artista, todos los datos de la canción dentro del body, y también el archivo que el usuario suba en el input cancion
     * @param {res} res Permite redireccionar a la ventana NuevaCancion en el caso de que algo haya salido mal, y si todo se ejecuta de manera correcta, nos redirecciona a la ventana Canciones
     */
    guardar_cancion(req, res) {
        var form = new formidable.IncomingForm();
        form.parse(req, function (err, fields, files) {
            var external = req.user.id;
            var artist = req.user.artista;
            var genero = fields.genero;
            var colaboradores = fields.colaboradores;
            var albums = fields.album;
            var nombre = fields.nombres;
            var extension = files.cancion.name.split(".").pop().toLowerCase();
            var nombreCancion = artist + "-" + nombre + '.' + extension;
            album.findOne({where: {nom_album: albums}, include: [{model: artista, as: 'artistum'}]}).then(function (datos) {
                var datosAlbum = {
                    nombre: nombre,
                    genero: genero,
                    colaborador: colaboradores,
                    direccion: nombreCancion,
                    reproducciones: 0,
                    id_album: datos.id,
                    external_id: uuid.v4()
                };
                cancion.create(datosAlbum).then(function (CancionDatos) {
                    var datosRelacion = {
                        id_albums: datos.id,
                        id_artistas: datos.artistum.id,
                        id_cancion: CancionDatos.id
                    };
                    art_alb_can.create(datosRelacion).then(function (CancionDatos) {
                        if (files.cancion.size <= maxSize) {

                            if (extensiones.includes(extension)) {

                                fs.rename(files.cancion.path, "public/canciones/" + nombreCancion,
                                        function (err) {
                                            if (err) {
                                                req.flash('error', "Hubo un error " + err);
                                                res.redirect("/nueva_cancion");
                                            } else {
                                                cancion.findOne({where: {nombre: nombre}}).then(function (result) {
                                                    result.direccion = nombreCancion;
                                                    result.save().then(function (ok) {
                                                        fs.exists(files.cancion.path, function (exists) {
                                                            if (exists)
                                                                fs.unlinkSync(files.cancion.path);
                                                        });
                                                        req.flash('info', "Se ha subido correctamente la canción");
                                                        res.redirect("/cancion");
                                                    });
                                                });
                                            }

                                        });
                            } else {
                                req.flash('error', "El tipo de archivo no es valido, debe ser mp3, flac, m4a, wav");
                                res.redirect("/nueva_cancion");
                            }
                        } else {
                            req.flash('error', "El tamano no puede superar los 25 MB");
                            res.redirect("/nueva_cancion");
                        }
                    });
                });
            });
        });

    }
}
module.exports = ControladorCancion;
