'use strict';
module.exports = (sequelize, DataTypes) => {
  const cancion = sequelize.define('cancion', {
    nombre: DataTypes.STRING,
    genero: DataTypes.STRING,
    colaborador: DataTypes.STRING,
    direccion: DataTypes.STRING,
    reproducciones: DataTypes.INTEGER,
    external_id: DataTypes.UUID
  }, {freezeTableName: true});
  cancion.associate = function(models) {
      cancion.hasMany(models.art_alb_can, {foreignKey: 'id_cancion', as: 'cancion'});
      cancion.hasMany(models.lista_cancion , {foreignKey: 'id_canciones', as: 'canciones'});
      cancion.belongsTo(models.album, {foreignKey: 'id_album'});
      
  };
  return cancion;
  
};

