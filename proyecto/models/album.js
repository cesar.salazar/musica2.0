'use strict';
module.exports = (sequelize, DataTypes) => {
  const album = sequelize.define('album', {
    nom_album: DataTypes.STRING,
    portada: DataTypes.STRING,
    external_id: DataTypes.UUID
  }, {freezeTableName: true});
  album.associate = function(models) {
      album.hasMany(models.art_alb_can, {foreignKey: 'id_albums', as: 'albums'});
      album.hasMany(models.cancion, {foreignKey: 'id_album', as: 'album'});
      album.belongsTo(models.artista, {foreignKey: 'id_artista'});
  };
  return album;
  
};


