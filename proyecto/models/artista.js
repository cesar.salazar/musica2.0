'use strict';
module.exports = (sequelize, DataTypes) => {
  const artista = sequelize.define('artista', {
    nom_artista: DataTypes.STRING,
    tipo_artista: DataTypes.STRING,
    external_id: DataTypes.UUID
  }, {freezeTableName: true});
  artista.associate = function(models) {
      artista.hasMany(models.art_alb_can, {foreignKey: 'id_artistas', as: 'artistas'});
      artista.belongsTo(models.persona, {foreignKey: 'id_persona'});
      artista.hasMany(models.album, {foreignKey: 'id_artista', as: 'artista'});
  };
  return artista;
  
};


