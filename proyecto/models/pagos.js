'use strict';
module.exports = (sequelize, DataTypes) => {
    const pagos = sequelize.define('pagos', {
        costo: DataTypes.STRING,
        fecha: DataTypes.DATEONLY,
        nombre_plan: DataTypes.STRING,
        external_id: DataTypes.UUID
    }, {freezeTableName: true});
    pagos.associate = function (models) {
        pagos.belongsTo(models.persona, {foreignKey: 'id_persona'});
    };
    return pagos;

};
