'use strict';
module.exports = (sequelize, DataTypes) => {
  const rol = sequelize.define('rol', {
    nombre_rol: DataTypes.STRING
  }, {freezeTableName: true});
  rol.associate = function(models) {
      rol.hasMany(models.persona, {foreignKey: 'id_rol', as: 'persona'});
  };
  return rol;

  
};


