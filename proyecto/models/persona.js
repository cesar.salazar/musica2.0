'use strict';
module.exports = (sequelize, DataTypes) => {
  const persona = sequelize.define('persona', {
    apellidos: DataTypes.STRING(40),
    nombres: DataTypes.STRING(40),
    fecha_nac: DataTypes.DATEONLY,
    telefono: DataTypes.STRING,
    foto: DataTypes.STRING(),
    external_id: DataTypes.UUID
  }, {freezeTableName: true});
  persona.associate = function(models) {
      persona.hasOne(models.usuario, {foreignKey: 'id_persona', as: 'usuario'});
      persona.belongsTo(models.rol, {foreignKey: 'id_rol'});
      persona.hasOne(models.artista, {foreignKey: 'id_persona', as: 'artista'});
      persona.hasMany(models.lista_reprod, {foreignKey: 'id_persona', as: 'list_reprod'});
      persona.hasMany(models.pagos,{foreignKey: 'id_persona', as: 'pagos'});
  };
  return persona;

  
};


