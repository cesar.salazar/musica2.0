'use strict';
module.exports = (sequelize, DataTypes) => {
  const lista_reprod = sequelize.define('lista_reprod', {
    nom_lista: DataTypes.STRING,    
    descripcion: DataTypes.STRING,
    caratula:DataTypes.STRING,
    external_id: DataTypes.UUID
  }, {freezeTableName: true});
  lista_reprod.associate = function(models) {
      lista_reprod.hasMany(models.lista_cancion , {foreignKey: 'id_listaR', as: 'lista_reprod'});
      lista_reprod.belongsTo(models.persona, {foreignKey: 'id_persona'});
  };
  return lista_reprod;
  

};


