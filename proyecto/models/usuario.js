'use strict';
module.exports = (sequelize, DataTypes) => {
  const usuario = sequelize.define('usuario', {
    correo: DataTypes.STRING,
    clave: DataTypes.STRING,
    estado: DataTypes.BOOLEAN,
    external_id: DataTypes.UUID
  }, {freezeTableName: true});
  usuario.associate = function(models) {
      usuario.belongsTo(models.persona, {foreignKey: 'id_persona'});
  };
  return usuario;

  
};


