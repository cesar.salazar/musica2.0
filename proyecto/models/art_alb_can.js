'use strict';
module.exports = (sequelize, DataTypes) => {
  const art_alb_can = sequelize.define('art_alb_can', {
  }, {freezeTableName: true});
  art_alb_can.associate = function(models) {
      art_alb_can.belongsTo(models.artista, {foreignKey: 'id_artistas', through: 'art_alb_can'});
      art_alb_can.belongsTo(models.album, {foreignKey: 'id_albums', through: 'art_alb_can'});
      art_alb_can.belongsTo(models.cancion, {foreignKey: 'id_cancion', through: 'art_alb_can'});
  };
  return art_alb_can;
  
};


