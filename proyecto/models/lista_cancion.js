'use strict';
module.exports = (sequelize, DataTypes) => {
  const lista_cancion = sequelize.define('lista_cancion', {
  }, {freezeTableName: true});
  lista_cancion.associate = function(models) {
      lista_cancion.belongsTo(models.cancion, {foreignKey: 'id_canciones', through: 'lista_cancion'});
      lista_cancion.belongsTo(models.lista_reprod, {foreignKey: 'id_listaR', through: 'lista_cancion'});
  };
  return lista_cancion;

  
};


